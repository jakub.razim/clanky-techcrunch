import React, {Component} from "react";
import {Container, Table} from "react-bootstrap";
import {QueryClient, QueryClientProvider} from "react-query";
import {TopNavigation} from "./TopNavigation";
import {Articles} from "./Articles";
import {BottomNavigation} from "./BottomNavigation";

class Content extends Component {
  constructor(props) {
    super(props);
    this.queryClient = new QueryClient();

    this.state = {
      queryParams: {
        currentPage: 1,
        numberOfResults: 20,
        orderBy: 'date',
        order: 'desc'
      },
      searchString: ''
    }

  }

  handlePage = (newPage) => {
    this.setState(prevState => ({
      queryParams: {
        ...prevState.queryParams,
        currentPage: newPage
      }
    }));
  }

  handleChangeNoR = (newNumber) => {
    this.setState(prevState => ({
      queryParams: {
        ...prevState.queryParams,
        numberOfResults: newNumber
      }
    }));
  }

  handleOrderBy = (newOrderBy) => {
    this.setState(prevState => ({
      queryParams: {
        ...prevState.queryParams,
        orderBy: newOrderBy,
        order: 'desc'
      }
    }));
  }

  handleSearchString = (newString) => {
    this.setState({
      searchString : newString
    })
  }

  toggleOrder = () => {
    this.setState(prevState => ({
      queryParams: {
        ...prevState.queryParams,
        order: this.state.queryParams.order === 'desc' ? 'asc' : 'desc'
      }
    }));
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(prevProps.currentRoute !== this.props.currentRoute){
      this.setState({
        queryParams: {
          ...prevState.queryParams,
          currentPage: 1,
          numberOfResults: 20,
        }
      })
    }
  }

  render() {
    return (
      <Container className="mt-3 mb-5">
        <TopNavigation handleOrderBy={this.handleOrderBy} toggleOrder={this.toggleOrder} handleSearchString={this.handleSearchString} order={this.state.queryParams.order} searchString={this.state.searchString}/>
        <Table striped hover responsive size="sm">
          <thead>
          <tr>
            <th>ID</th>
            <th>Titulek</th>
            <th>Datum</th>
            <th>Odkaz</th>
            <th>Autor</th>
          </tr>
          </thead>
          <tbody>
            <QueryClientProvider client={this.queryClient}>
              <Articles currentRoute={this.props.currentRoute} queryParams={this.state.queryParams} searchString={this.state.searchString}/>
            </QueryClientProvider>
          </tbody>
        </Table>
        <BottomNavigation handlePage={this.handlePage} handleChangeNoR={this.handleChangeNoR} queryParams={this.state.queryParams}/>
      </Container>
    );
  }

}

export default Content;