import {Container, Navbar, Nav} from "react-bootstrap";

const SimplePagination = props => {
    const isFirstPage = props.currentPage === 1;
    const pageNumbers = isFirstPage ? [props.currentPage, props.currentPage + 1, props.currentPage +2] : [props.currentPage -1, props.currentPage, props.currentPage + 1];

    return (
      <Nav>
        {!isFirstPage &&
          <Nav.Link onClick={() => props.handlePage(1)} active={isFirstPage}>
            Na začátek
          </Nav.Link>
        }
        <Nav.Link onClick={() => props.handlePage(pageNumbers[0])} active={isFirstPage}>
          {pageNumbers[0]}
        </Nav.Link>
        <Nav.Link onClick={() => props.handlePage(pageNumbers[1])} active={!isFirstPage}>
          {pageNumbers[1]}
        </Nav.Link>
        <Nav.Link onClick={() => props.handlePage(pageNumbers[2])}>
          {pageNumbers[2]}
        </Nav.Link>
      </Nav>
    );
}

export const BottomNavigation = props => {

  return (
    <Navbar>
      <Container>
        <SimplePagination currentPage={props.queryParams.currentPage} handlePage={props.handlePage}/>
        <Nav>
          {[20, 50, 100].map((number) => {
            return (
              <Nav.Link key={'res_' + number} onClick={() => props.handleChangeNoR(number)} active={props.queryParams.numberOfResults === number}>
                {number}
              </Nav.Link>
            )
          })}
        </Nav>
      </Container>
    </Navbar>
  );
}