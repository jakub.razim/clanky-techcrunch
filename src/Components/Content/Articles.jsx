import React, {useState} from "react";
import he from "he";
import {useQuery} from "react-query";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStar as faStarRegular} from "@fortawesome/free-regular-svg-icons";
import {faStar as faStarSolid} from "@fortawesome/free-solid-svg-icons";
import {ArticlesWentWrong} from "../Placeholders";

export function Articles(props) {
  const [favouriteItems, setFavouriteItems] = useState(JSON.parse(localStorage.getItem('clankyTechcrunchFavouritesList')) || []);
  const querySearch = props.searchString !== '' ? '&search=' + encodeURIComponent(props.searchString) : '';
  const queryFavouriteItems = props.currentRoute === '/favourites' && favouriteItems.length > 0 ? '&include[]=' + favouriteItems.join('&include[]=') : '';

  const {isLoading, error, data} = useQuery(['articles', props.searchString, favouriteItems, props.currentRoute, props.queryParams.currentPage, props.queryParams.numberOfResults, props.queryParams.orderBy, props.queryParams.order], () =>
    fetch(`https://techcrunch.com/wp-json/wp/v2/posts?${querySearch + queryFavouriteItems}&_fields=id,title.rendered,date,link,parsely.meta.author&page=${props.queryParams.currentPage}&per_page=${props.queryParams.numberOfResults}&orderby=${props.queryParams.orderBy}&order=${props.queryParams.order}&context=embed`).then(res =>
      res.json()
    )
  )

  if(isLoading) return <ArticlesWentWrong message='Načítání...'/>;
  if(error) return <ArticlesWentWrong message='Něco se pokazilo...'/>;
  if(data.length === 0 || (props.currentRoute === '/favourites' && favouriteItems.length === 0)) return <ArticlesWentWrong message='Žádné výsledky'/>;

  function setFavouriteItemsGlobal(newItem){
    const updatedList = !favouriteItems.includes(newItem) ? [...favouriteItems, newItem] : favouriteItems.filter((value) => {
      return value!==newItem;
    });

    localStorage.setItem('clankyTechcrunchFavouritesList', JSON.stringify(updatedList));
    setFavouriteItems(updatedList);
  }

  return (
    <React.Fragment>
      {data.map((el) => {
        return (
          <tr key={el.id}>
            <td>{el.id}</td>
            <td>{he.decode(el.title.rendered)}</td>
            <td>{el.date.split("T")[0]}</td>
            <td><a href={el.link} target="_blank" rel="noopener noreferrer">{el.link}...</a></td>
            <td>{el.parsely.meta.author.length > 0 ? el.parsely.meta.author[0].name : 'Neznámý autor'}</td>
            <td><FontAwesomeIcon className="fav-icon" icon={favouriteItems.includes(el.id) ? faStarSolid : faStarRegular} onClick={() => setFavouriteItemsGlobal(el.id)}/></td>
          </tr>
        )
      })}
    </React.Fragment>
  )

}