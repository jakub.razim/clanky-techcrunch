import {Navbar, Container, Nav, Form, FormControl, Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faXmark, faArrowDown, faArrowUp} from "@fortawesome/free-solid-svg-icons";


export const TopNavigation = props => {
  return (
    <Navbar>
      <Container>
        <Nav>
          <Form>
            <FormControl
              type="search"
              placeholder="Vyhledat"
              onChange={s => props.handleSearchString(s.target.value)}
              value={props.searchString}
            />
          </Form>
          <Button variant="light" className="mx-1" onClick={() => props.handleSearchString('')}><FontAwesomeIcon icon={faXmark}/></Button>
        </Nav>
        <Nav>
          <Form.Select onChange={o => props.handleOrderBy(o.target.value)}>
            <option value="date">Datum</option>
            <option value="title">Titulek</option>
            <option value="author">Autor</option>
          </Form.Select>
          <Button variant="light" className="mx-1" onClick={() => props.toggleOrder()}><FontAwesomeIcon icon={props.order === 'desc' ? faArrowDown : faArrowUp}/></Button>
        </Nav>
      </Container>
    </Navbar>
  );
}