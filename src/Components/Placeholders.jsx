import React from "react";

export const ArticlesWentWrong = props => {
  return (
    <tr>
      <td/>
      <td className="articles-went-wrong">{props.message}</td>
      <td/>
      <td/>
      <td/>
    </tr>
  )
}