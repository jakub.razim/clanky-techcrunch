import React from 'react';
import {Navbar, Container, Nav} from "react-bootstrap";

export const Menu = props => {

  return (
    <Navbar bg="light" expand="sm">
      <Container>
        <Nav className="mx-auto">
          <Nav.Link active={props.currentRoute === '/'} onClick={() => props.router("/")}>Všechny články</Nav.Link>
          <Nav.Link active={props.currentRoute === '/favourites'} onClick={() => props.router("/favourites")}>Oblíbené články</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};