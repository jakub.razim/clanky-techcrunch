import "./App.css";
import {Component} from "react";
import {Menu} from "./Components/Menu";
import Content from "./Components/Content/Content";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentRoute: "/"
    }
  }

  sectionRouter = (route) => {
    this.setState({
      currentRoute: route
    })
  }

  render() {
    return (
      <div className="App">
        <Menu router={this.sectionRouter} currentRoute={this.state.currentRoute}/>
        <Content currentRoute={this.state.currentRoute}/>
      </div>
    );
  }
}

export default App;